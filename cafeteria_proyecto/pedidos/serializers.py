from rest_framework import serializers
from .models import Pedido
import json

class PedidoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pedido
        fields = ['id_pedido', 'id_cliente', 'mesa', 'lista_productos', 'id_estado', 'fecha_carga']
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['lista_productos'] = json.dumps(representation['lista_productos'])
        return representation