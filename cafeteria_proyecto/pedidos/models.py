from django.db import models
from django.utils import timezone
import datetime


class Pedido(models.Model):
    id_pedido = models.models.AutoField(primary_key=True)
    id_cliente = models.IntegerField()
    mesa = models.IntegerField()
    lista_productos = models.JSONField()
    id_estado = models.IntegerField()
    fecha_carga = models.DateTimeField(auto_now_add=True)
    
    
    
    def __str__(self) -> str:
        return str(self.mesa)