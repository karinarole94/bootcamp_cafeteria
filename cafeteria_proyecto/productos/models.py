from django.db import models

class Producto(models.Model):
    id_producto = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    precio = models.IntegerField(default=0)
    

    def __str__(self) -> str:
        return self.nombre

